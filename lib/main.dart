import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final list = List.generate(50, (index) => GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => DetailScreen(index: index))),
      child: Hero(
        tag: 'imageHero-${index}',
        child: Image.network(
          'https://picsum.photos/id/${index}/200/300',
          width: 100,
        ),
      ),
    ));

    return Scaffold(
      appBar: AppBar(
        title: Text('Main Screen'),
      ),
      body: SingleChildScrollView(
        child: Wrap(
          children: list,
        ),
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  final int index;

  const DetailScreen({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Hero(
            tag: 'imageHero-${index}',
            child: Image.network(
              'https://picsum.photos/id/${index}/200/300',
              width: 200,
            ),
          )
          ,
        ),
      ),
    );
  }
}
